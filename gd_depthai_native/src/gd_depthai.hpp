#ifndef GD_DEPTHAI_HPP
#define GD_DEPTHAI_HPP

#include <Godot.hpp>
#include <Node.hpp>
#include <Spatial.hpp>
#include <TextureRect.hpp>
#include <ImageTexture.hpp>
#include <VBoxContainer.hpp>
#include <HBoxContainer.hpp>
#include <MeshInstance.hpp>
#include <ShaderMaterial.hpp>

#include <Label.hpp>
#include <Slider.hpp>

#include <opencv2/opencv.hpp>

#include "depthai/depthai.hpp"

#include <map>

#define RGB_BPP 3
#define DEPTH_BPP 2

static bool rawToBool(float f);
static int rawToISO(float f);
static dai::StereoDepthConfig::MedianFilter rawToDepthMedianKernel(float f);
static dai::RawStereoDepthConfig::PostProcessing::TemporalFilter::PersistencyMode rawToPersistencyMode(float f);

class CameraControl;

typedef struct nnConfig {
    std::string modelPath;
    std::vector<std::string> labels;
    uint32_t inputW;
    uint32_t inputH;
} NNConfig;

typedef struct cameraParamsRGB {
    bool autoExposure;
    uint32_t exposureMs;
    uint32_t exposureISO;
    float flat_intrinsics[4];
} CameraParamsRGB;

typedef struct cameraParamsMono {
    bool autoExposure;
    uint32_t exposureMs;
    uint32_t exposureISO;
    float flat_intrinsics[4];
} CameraParamsMono;

typedef struct cameraParamsDepth {
    uint32_t confidence;
    uint8_t median_filter;
    uint8_t lrcheck;
    uint8_t subpixel;
    uint8_t spatial_radius;
    uint8_t spatial_iterations;
    uint8_t speckle_range;
    uint8_t decimation_factor;
    uint8_t temporal_filter;
    uint8_t census_mean;
    float census_alpha;
    float census_beta;
    float threshold_filter_min;
    float threshold_filter_max;

} CameraParamsDepth;


typedef enum viewId {
    VIEW_ID_NONE=0,
    VIEW_ID_LEFT=1,
    VIEW_ID_RIGHT=2,
    VIEW_ID_RGB=4,
} ViewId;

typedef struct cvCameraParams {
    cv::Mat calcIntrinsics;
    cv::Mat calcDistortion;
} CVCameraParams;

typedef struct cvStereoCameraParams {
    ViewId primary;
    ViewId secondary;
} CVStereoCameraParams;


typedef struct depthAIConfig {
    uint32_t rgbW;
    uint32_t rgbH;
    uint32_t monoW;
    uint32_t monoH;
    bool hasRGB;
    bool hasMono;
    bool hasDepth;
    bool alignDepthToRGB;
    bool hasFeatureTracking;
    bool hasObjectTracking;
    //float monoBaseline = 0.08; //hardcoded for OAK-D right now
    float monoBaseline = 0.075; //hardcoded for depthai lite right now
    std::vector<CameraControl> cameraControls;
    CameraParamsRGB rgbCameraParams;
    CameraParamsMono lMonoCameraParams;
    CameraParamsMono rMonoCameraParams;
    CameraParamsDepth depthCameraParams;
    CVCameraParams lMonoCVParams;
    CVCameraParams rMonoCVParams;
    CVCameraParams rgbMonoCVParams;
    std::vector<CVStereoCameraParams> stereoPairs;
    NNConfig nnConfig;

}
DepthAIConfig;



typedef struct trackPoint3{
    uint64_t timestamp;
    godot::Vector3 position;
} TrackPoint3;

typedef struct stereoFeature {
    uint32_t lId;
    uint32_t rId;
    godot::Vector2 lPoint;
    godot::Vector2 rPoint;
    float disparity;
    float depth;
    TrackPoint3 cur;
    bool updateHist; //used as a flag to make sure we only append to history once
    uint32_t lostFrames; //used as a counter to remove lost tracks
    std::vector<TrackPoint3> history;
    uint32_t markerIndex;
} StereoFeature;

namespace godot {

class GDDepthAI : public Spatial {
    GODOT_CLASS(GDDepthAI, Spatial)

private:
    bool depthaiConfigured;
    bool depthaiConfigurationFailed;
    bool meshInTree;

    //depthAI devices and queues

    dai::Device* daiDevice;
    dai::Pipeline pipeline;
    std::vector<std::vector<float>> daiRGBIntrinsics;
    std::vector<std::vector<float>> daiMonoIntrinsics[2];

    dai::DataOutputQueue* daiRGBQueue;
    dai::DataOutputQueue* daiMonoQueue[2];
    dai::DataOutputQueue* daiDepthQueue;
    dai::DataOutputQueue* daiLTrackerQueue;
    dai::DataOutputQueue* daiRTrackerQueue;
    dai::DataOutputQueue* daiObjectTrackerQueue;

    Ref<ImageTexture> daiRGBTex;
    Ref<Image> daiRGBImg;
    PoolByteArray daiRGBPBA;

    Ref<ImageTexture> daiLMonoTex;
    Ref<Image> daiLMonoImg;
    PoolByteArray daiLMonoPBA;

    Ref<ImageTexture> daiRMonoTex;
    Ref<Image> daiRMonoImg;
    PoolByteArray daiRMonoPBA;

    Ref<ImageTexture> daiDepthTex;
    Ref<Image> daiDepthImg;
    PoolByteArray daiDepthPBA;

    cv::Mat cvNullMonoMat;

    cv::Mat cvFeatureDebugMat;
    Ref<ImageTexture> cvFeatureDebugTex;
    Ref<Image> cvFeatureDebugImg;
    PoolByteArray cvFeatureDebugPBA;

    cv::Mat cvObjectDebugMat;
    Ref<ImageTexture> cvObjectDebugTex;
    Ref<Image> cvObjectDebugImg;
    PoolByteArray cvObjectDebugPBA;

    Ref<ImageTexture> nnTex;
    Ref<Image> nnImg;
    PoolByteArray nnPBA;

    cv::Mat cvCalibMat;
    Ref<ImageTexture> cvCalibTex;
    Ref<Image> cvCalibImg;
    PoolByteArray cvCalibPBA;

    std::vector<uint32_t> freeMarkerList;

    cv::Size chessboardSize;
    std::vector<std::vector<cv::Point3f>> lMonoBoardCorners;
    std::vector<std::vector<cv::Point3f>> rMonoBoardCorners;
    std::vector<std::vector<cv::Point3f>> rgbBoardCorners;
    std::vector<std::vector<cv::Point2f>> lMonoScreenCorners;
    std::vector<std::vector<cv::Point2f>> rMonoScreenCorners;
    std::vector<std::vector<cv::Point2f>> rgbScreenCorners;
    uint32_t capturedSamples;

    VBoxContainer* getCalibrationControlContainer(); //for GDScript calibrator
    void clearChessboardData();
    void detectAndAppendChessboard(uint8_t views);
    void solveDistortion(uint8_t views);
    Dictionary getSolvedIntrinsics(uint8_t view);
    Dictionary getSolvedDistortion(uint8_t view);

    bool reconfigureDepthAI(DepthAIConfig* daiConfig);
    void createPointcloudMesh(uint32_t w, uint32_t h);
    void createTrackingMarkers(uint32_t n);
    void createObjectMarkers(uint32_t n);
    bool getFreeMarkerIndex(uint32_t* out_i);
    uint32_t calculateSAD(cv::Mat prv,cv::Mat tst);
    float subPixelRefine(cv::Mat r_line,cv::Mat l_line,int dist);

    void findCorrespondencesSAD(std::vector<dai::TrackedFeature>* rFeatures,std::map<uint32_t,StereoFeature>* out_features);
    void drawStereoFeatureDebug(cv::Mat m,std::map<uint32_t,StereoFeature>* features);
    void positionMarkers(std::map<uint32_t,StereoFeature>* features);
    void positionObjectMarkers();
public:
    static void _register_methods();

	GDDepthAI();
	~GDDepthAI();

	void _init(); // our initializer called by Godot
    void _ready(); // our initializer called by Godot

	void _process(float delta);

    DepthAIConfig depthAIConfig;
    dai::DataInputQueue* daiRGBControlQueue;
    dai::DataInputQueue* daiRGBConfigQueue;
    dai::DataInputQueue* daiMonoControlQueue;
    dai::DataInputQueue* daiDepthControlQueue;
    dai::DataInputQueue* daiTrackerConfigQueue;


    MeshInstance* pointcloudQuads;
    Ref<ShaderMaterial> pointcloudShaderMaterial;

    void setControlSliderValue(float f,int i);

    Ref<ImageTexture> getDepthAITex(String s);

    std::map<uint32_t,StereoFeature> stereoFeatures;
    std::vector<MeshInstance*> markers;

    VBoxContainer* calibrationControlContainer;
};

}
typedef enum controlType {
    CONTROL_TYPE_NONE,
    CONTROL_TYPE_RGB_CONFIG,
    CONTROL_TYPE_RGB_CONTROL,
    CONTROL_TYPE_MONO_CONTROL,
    CONTROL_TYPE_DEPTH_CONFIG,
    CONTROL_TYPE_POINTCLOUD,
    CONTROL_TYPE_FEATURE_TRACKING,
    CONTROL_TYPE_OBJECT_TRACKING,
} ControlType;

typedef enum controlId {
    CONTROL_ID_NONE,
    CONTROL_ID_EXP_MS,
    CONTROL_ID_EXP_ISO,
    CONTROL_ID_DEPTH_CONF,
    CONTROL_ID_DEPTH_LR_CHECK,
    CONTROL_ID_DEPTH_SUBPIXEL,
    CONTROL_ID_DEPTH_MEDIAN,
    CONTROL_ID_DEPTH_CENSUS_MEAN_ENABLE,
    CONTROL_ID_DEPTH_COSTMATCH_ALPHA,
    CONTROL_ID_DEPTH_COSTMATCH_BETA,
    CONTROL_ID_DEPTH_SPECKLE,
    CONTROL_ID_DEPTH_SPATIAL_ENABLE,
    CONTROL_ID_DEPTH_SPATIAL_RADIUS,
    CONTROL_ID_DEPTH_SPATIAL_ITERATIONS,
    CONTROL_ID_DEPTH_DECIMATION,
    CONTROL_ID_DEPTH_TEMPORAL_FILTER,
    CONTROL_ID_DEPTH_THRESHOLD_MIN,
    CONTROL_ID_DEPTH_THRESHOLD_MAX,
    CONTROL_ID_POINTCLOUD_POINTSIZE,
    CONTROL_ID_FEATURE_TRACKING_ALGORITHM,
    CONTROL_ID_OBJECT_TRACKING_ROI,

} ControlId;

class CameraControl {
public:
    CameraControl(ControlType _t, ControlId _id,float _min, float _max, float _steps,float _default,std::string _controlName,std::string _controlLabel,std::string _valueLabel);
    ControlType type;
    ControlId id;
    bool enabled;
    float value;
    float defaultValue;
    float min;
    float max;
    float steps;
    bool dirty;
    std::string controlName;
    std::string controlLabel;
    std::string formattedValue;

    float getValue();
    void setValue(godot::GDDepthAI* d,float f);
};

class SelectionControl {
public:
    SelectionControl(ControlType _t, ControlId _id,uint32_t defaultIndex,std::vector<std::string> _elements,std::string _controlName,std::string _controlLabel,std::string _valueLabel);
    ControlType type;
    ControlId id;
    bool enabled;
    std::vector<std::string> elements;
    uint32_t selectedIndex;
    bool dirty;
    std::string controlName;
    std::string controlLabel;
    std::string formattedValue;
    void setValue(godot::GDDepthAI* d,uint32_t index);
};

#endif
