#include "gd_depthai.hpp"

#include <ScrollContainer.hpp>
#include <HSlider.hpp>
#include <Button.hpp>

#include <ArrayMesh.hpp>
#include <Shader.hpp>
#include <Color.hpp>
#include <ResourceLoader.hpp>
#include <AABB.hpp>
#include <CubeMesh.hpp>
#include <SpatialMaterial.hpp>

using namespace godot;

extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o) {
    godot::Godot::gdnative_init(o);
}

extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o) {
	godot::Godot::gdnative_terminate(o);
}

extern "C" void GDN_EXPORT godot_nativescript_init(void *handle) {
	godot::Godot::nativescript_init(handle);
	godot::register_class<godot::GDDepthAI>();
}

void GDDepthAI::_register_methods() {
    register_method("_process", &GDDepthAI::_process);
    register_method("_ready", &GDDepthAI::_ready);
    register_method("getDepthAITex", &GDDepthAI::getDepthAITex);
    register_method("setControlSliderValue",&GDDepthAI::setControlSliderValue);
    register_method("getCalibrationControlContainer",&GDDepthAI::getCalibrationControlContainer);
    register_method("detectAndAppendChessboard",&GDDepthAI::detectAndAppendChessboard);
    register_method("solveDistortion",&GDDepthAI::solveDistortion);
    register_method("getSolvedIntrinsics",&GDDepthAI::getSolvedIntrinsics);
    register_method("getSolvedDistortion",&GDDepthAI::getSolvedDistortion);
}


VBoxContainer* GDDepthAI::getCalibrationControlContainer() {
    return calibrationControlContainer;
}

void GDDepthAI::detectAndAppendChessboard(uint8_t views) {

    chessboardSize = cv::Size(8,6);
    float squareSize = 0.25f;
    uint32_t flags = (cv::CALIB_CB_NORMALIZE_IMAGE | cv::CALIB_CB_ADAPTIVE_THRESH);

    //TODO - move this into its own function, we only need to do this once
    std::vector<cv::Point3f> boardPoints;
    for (uint32_t i=0;i<chessboardSize.width;i++) {
        for (uint32_t j=0;j<chessboardSize.height;j++) {
            boardPoints.push_back(cv::Point3f(i*squareSize,j*squareSize,0.0f));
        }
    }

    bool foundLeft = false;
    bool foundRight = false;
    bool foundRGB = false;

    std::vector<cv::Point2f> lMonoScreenPoints;
    std::vector<cv::Point2f> rMonoScreenPoints;
    std::vector<cv::Point2f> rgbScreenPoints;

    if (views & ViewId::VIEW_ID_LEFT) {
        PoolByteArray::Read r = daiLMonoPBA.read();
        cv::Mat m(depthAIConfig.monoH,depthAIConfig.monoW,CV_8UC1,(void*)r.ptr());
        cvNullMonoMat = cv::Mat(depthAIConfig.monoH,depthAIConfig.monoW,CV_8UC1,cv::Scalar(0));
        if (cv::findChessboardCorners(m,chessboardSize,lMonoScreenPoints,flags)) {
            foundLeft = true;
        }
    }
    if (views & ViewId::VIEW_ID_RIGHT) {
        std::vector<cv::Point2f> corners;
        PoolByteArray::Read r = daiRMonoPBA.read();
        cv::Mat m(depthAIConfig.monoH,depthAIConfig.monoW,CV_8UC1,(void*)r.ptr());
        if (cv::findChessboardCorners(m,chessboardSize,rMonoScreenPoints,flags)) {
            foundRight = true;
        }
    }
    if (views & ViewId::VIEW_ID_RGB) {
        std::vector<cv::Point2f> corners;
        PoolByteArray::Read r = daiRGBPBA.read();
        cv::Mat m(depthAIConfig.rgbH,depthAIConfig.rgbW,CV_8UC3,(void*)r.ptr());
        if (cv::findChessboardCorners(m,chessboardSize,rgbScreenPoints,flags)) {
            foundRGB = true;
        }
    }

    bool foundAll = true;
    if ((views & ViewId::VIEW_ID_LEFT) && (! foundLeft)) {
        foundAll = false;
    }
    if ((views & ViewId::VIEW_ID_RIGHT) && (! foundRight)) {
        foundAll = false;
    }
    if ((views & ViewId::VIEW_ID_RGB) && (! foundRGB)) {
        foundAll = false;
    }

    if (foundAll) {
        capturedSamples++;
        //append our points
        if (foundLeft) {
            lMonoBoardCorners.push_back(boardPoints);
            lMonoScreenCorners.push_back(lMonoScreenPoints);
        }
        if (foundRight) {
            rMonoBoardCorners.push_back(boardPoints);
            rMonoScreenCorners.push_back(rMonoScreenPoints);
        }
        if (foundRGB) {
            rgbBoardCorners.push_back(boardPoints);
            rgbScreenCorners.push_back(rgbScreenPoints);
        }

    }
}
void GDDepthAI::solveDistortion(uint8_t views) {
    std::vector<cv::Mat> tvecs;
    std::vector<cv::Mat> rvecs;
    uint32_t flags = 0;
    float rmsError = cv::calibrateCamera(lMonoBoardCorners,lMonoScreenCorners,cv::Size(depthAIConfig.monoW,depthAIConfig.monoH),depthAIConfig.lMonoCVParams.calcIntrinsics,depthAIConfig.lMonoCVParams.calcDistortion,rvecs,tvecs,flags);
    printf("L MONO CALIBRATION RMS ERROR: %f\n",rmsError);
}
void GDDepthAI::clearChessboardData() {
    capturedSamples = 0;
    lMonoBoardCorners.clear();
    rMonoBoardCorners.clear();
    rgbBoardCorners.clear();
    lMonoScreenCorners.clear();
    rMonoScreenCorners.clear();
    rgbScreenCorners.clear();
}

Dictionary GDDepthAI::getSolvedIntrinsics(uint8_t view) {
    return Dictionary();

}
Dictionary GDDepthAI::getSolvedDistortion(uint8_t view) {
    return Dictionary();
}

GDDepthAI::GDDepthAI() {
}

GDDepthAI::~GDDepthAI() {
	// add your cleanup here
}

void GDDepthAI::_init() {
    // initialize any variables here
    depthaiConfigured=false;
    depthaiConfigurationFailed=false;
    meshInTree = false;

    daiRGBConfigQueue = NULL;
    daiRGBControlQueue = NULL;

    daiMonoControlQueue = NULL;
    daiDepthControlQueue = NULL;

    depthAIConfig.alignDepthToRGB = false;
    depthAIConfig.hasDepth = true;
    depthAIConfig.hasRGB = true;
    depthAIConfig.hasMono = true;
    depthAIConfig.hasFeatureTracking = true;
    depthAIConfig.hasObjectTracking = true;

    depthAIConfig.monoW = 640;
    depthAIConfig.monoH = 400;
    depthAIConfig.rgbW = 1920;
    depthAIConfig.rgbH = 1080;

    depthAIConfig.nnConfig.modelPath = "/Users/pete/Downloads/mobilenet-ssd_openvino_2021.2_6shave.blob";
    depthAIConfig.nnConfig.labels.push_back("background");
    depthAIConfig.nnConfig.labels.push_back("aeroplane");
    depthAIConfig.nnConfig.labels.push_back("bicycle");
    depthAIConfig.nnConfig.labels.push_back("bird");
    depthAIConfig.nnConfig.labels.push_back("boat");
    depthAIConfig.nnConfig.labels.push_back("bottle");
    depthAIConfig.nnConfig.labels.push_back("bus");
    depthAIConfig.nnConfig.labels.push_back("car");
    depthAIConfig.nnConfig.labels.push_back("cat");
    depthAIConfig.nnConfig.labels.push_back("chair");
    depthAIConfig.nnConfig.labels.push_back("cow");
    depthAIConfig.nnConfig.labels.push_back("dog");
    depthAIConfig.nnConfig.labels.push_back("horse");
    depthAIConfig.nnConfig.labels.push_back("motorbike");
    depthAIConfig.nnConfig.labels.push_back("person");
    depthAIConfig.nnConfig.labels.push_back("pottedplant");
    depthAIConfig.nnConfig.labels.push_back("sheep");
    depthAIConfig.nnConfig.labels.push_back("sofa");
    depthAIConfig.nnConfig.labels.push_back("train");
    depthAIConfig.nnConfig.labels.push_back("tvmonitor");

    depthAIConfig.nnConfig.inputW = 300;
    depthAIConfig.nnConfig.inputH = 300;

    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_RGB_CONTROL,CONTROL_ID_EXP_MS,0.0,33000.0,0.0,12000.0f,"rgb_exposure_ms","Exposure (ms)",""));
    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_RGB_CONTROL,CONTROL_ID_EXP_ISO,0.0,4.0,1.0,2.0f,"rgb_exposure_iso","Exposure (ISO)",""));

    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_MONO_CONTROL,CONTROL_ID_EXP_MS,0.0,33000.0,0.0,5000.0f,"mono_exposure_ms","Exposure (ms)",""));
    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_MONO_CONTROL,CONTROL_ID_EXP_ISO,0.0,4.0,1.0,2.0f,"mono_exposure_iso","Exposure (ISO)",""));

    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_DEPTH_CONFIG,CONTROL_ID_DEPTH_CONF,0.0,255.0,0.0,150.0f,"depth_confidence","Confidence",""));
    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_DEPTH_CONFIG,CONTROL_ID_DEPTH_CENSUS_MEAN_ENABLE,0.0,1.0,1.0,1.0f,"census_mean","Census Mean",""));
    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_DEPTH_CONFIG,CONTROL_ID_DEPTH_LR_CHECK,0.0,15.0,1.0,5.0f,"depth_lrcheck","LR Check",""));
    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_DEPTH_CONFIG,CONTROL_ID_DEPTH_SUBPIXEL,0.0,1.0,1.0,1.0f,"depth_subpixel","Subpixel",""));

    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_DEPTH_CONFIG,CONTROL_ID_DEPTH_MEDIAN,0.0,3.0,1.0,0.0f,"depth_median","Median Filter",""));

    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_DEPTH_CONFIG,CONTROL_ID_DEPTH_TEMPORAL_FILTER,0.0,8.0,1.0,0.0f,"temporal_filter","Temporal Filter",""));
    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_DEPTH_CONFIG,CONTROL_ID_DEPTH_SPECKLE,0.0,50.0,1.0,0.0f,"speckle_range","Speckle Range",""));

    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_DEPTH_CONFIG,CONTROL_ID_DEPTH_SPATIAL_RADIUS,0.0,10.0,1.0,0.0f,"spatial_radius","Hole-fill Radius",""));
    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_DEPTH_CONFIG,CONTROL_ID_DEPTH_SPATIAL_ITERATIONS,1.0,10.0,1.0,0.0f,"spatial_iterations","Hole-fill Iter.",""));

    //depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_DEPTH_CONFIG,CONTROL_ID_DEPTH_DECIMATION,1.0,4.0,1.0,1.0f,"decimation","Decimation",""));

    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_DEPTH_CONFIG,CONTROL_ID_DEPTH_THRESHOLD_MIN,0.0,16000.0,0.0,0.0f,"threshold_min","Threshold Min",""));
    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_DEPTH_CONFIG,CONTROL_ID_DEPTH_THRESHOLD_MAX,0.0,16000.0,0.0,16000.0f,"threshold_max","Threshold Max",""));

    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_POINTCLOUD,CONTROL_ID_POINTCLOUD_POINTSIZE,1.0,5.0,1.0,3.0f,"point_size","Point Size",""));

    depthAIConfig.cameraControls.push_back(CameraControl(CONTROL_TYPE_FEATURE_TRACKING,CONTROL_ID_FEATURE_TRACKING_ALGORITHM,0.0,1.0,1.0,0.0,"tracking_algo","Tracking Algo",""));

    daiLMonoTex = Ref<ImageTexture>(ImageTexture::_new());
    daiRMonoTex = Ref<ImageTexture>(ImageTexture::_new());
    daiDepthTex = Ref<ImageTexture>(ImageTexture::_new());
    daiDepthTex->set_flags(0);
    daiRGBTex = Ref<ImageTexture>(ImageTexture::_new());
    cvFeatureDebugTex = Ref<ImageTexture>(ImageTexture::_new());
    cvObjectDebugTex = Ref<ImageTexture>(ImageTexture::_new());
    nnTex = Ref<ImageTexture>(ImageTexture::_new());

    daiLMonoImg = Ref<Image>(Image::_new());
    daiRMonoImg = Ref<Image>(Image::_new());
    daiDepthImg = Ref<Image>(Image::_new());
    daiRGBImg = Ref<Image>(Image::_new());
    cvFeatureDebugImg = Ref<Image>(Image::_new());
    cvObjectDebugImg = Ref<Image>(Image::_new());
    nnImg = Ref<Image>(Image::_new());
    cvNullMonoMat = cv::Mat(depthAIConfig.monoH,depthAIConfig.monoW,CV_8UC1,cv::Scalar(0));
}

void GDDepthAI::_ready() {
    //construct our gui
    VBoxContainer* parent = VBoxContainer::_new();

    HBoxContainer* rgbCamContainer = HBoxContainer::_new();
    rgbCamContainer->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
    VBoxContainer* rgbControlContainer = VBoxContainer::_new();
    rgbControlContainer->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);

    HBoxContainer* monoCamContainer = HBoxContainer::_new();
    monoCamContainer->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
    VBoxContainer* monoControlContainer=VBoxContainer::_new();
    monoControlContainer->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);

    HBoxContainer* depthContainer = HBoxContainer::_new();
    depthContainer->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
    VBoxContainer* depthControlContainer=VBoxContainer::_new();
    depthControlContainer->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);

    HBoxContainer* featureTrackingContainer = HBoxContainer::_new();
    featureTrackingContainer->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
    VBoxContainer* featureTrackingControlContainer=VBoxContainer::_new();
    featureTrackingControlContainer->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);

    HBoxContainer* objectTrackingContainer = HBoxContainer::_new();
    objectTrackingContainer->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
    VBoxContainer* objectTrackingControlContainer=VBoxContainer::_new();
    objectTrackingControlContainer->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);


    HBoxContainer* calibrationContainer = HBoxContainer::_new();
    calibrationContainer->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
    calibrationControlContainer=VBoxContainer::_new();
    calibrationControlContainer->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);

    TextureRect* rgbTextureRect = TextureRect::_new();
    rgbTextureRect->set_texture(daiRGBTex);
    rgbTextureRect->set_expand(true);
    rgbTextureRect->set_custom_minimum_size(Vector2(0,100));
    rgbTextureRect->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
    rgbTextureRect->set_v_size_flags(Control::SizeFlags::SIZE_EXPAND);

    TextureRect* lMonoTextureRect = TextureRect::_new();
    lMonoTextureRect->set_texture(daiLMonoTex);
    lMonoTextureRect->set_expand(true);
    lMonoTextureRect->set_custom_minimum_size(Vector2(0,100));
    lMonoTextureRect->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
    lMonoTextureRect->set_v_size_flags(Control::SizeFlags::SIZE_EXPAND);

    TextureRect* rMonoTextureRect = TextureRect::_new();
    rMonoTextureRect->set_texture(daiRMonoTex);
    rMonoTextureRect->set_expand(true);
    rMonoTextureRect->set_custom_minimum_size(Vector2(0,100));
    rMonoTextureRect->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
    rMonoTextureRect->set_v_size_flags(Control::SizeFlags::SIZE_EXPAND);

    TextureRect* depthTextureRect = TextureRect::_new();
    depthTextureRect->set_texture(daiDepthTex);
    depthTextureRect->set_expand(true);
    depthTextureRect->set_custom_minimum_size(Vector2(0,100));
    depthTextureRect->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
    depthTextureRect->set_v_size_flags(Control::SizeFlags::SIZE_EXPAND);

    TextureRect* cvFeatureTextureRect = TextureRect::_new();
    cvFeatureTextureRect->set_texture(cvFeatureDebugTex);
    cvFeatureTextureRect->set_expand(true);
    cvFeatureTextureRect->set_custom_minimum_size(Vector2(0,100));
    cvFeatureTextureRect->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
    cvFeatureTextureRect->set_v_size_flags(Control::SizeFlags::SIZE_EXPAND);

    TextureRect* cvObjectTextureRect = TextureRect::_new();
    cvObjectTextureRect->set_texture(cvObjectDebugTex);
    cvObjectTextureRect->set_expand(true);
    cvObjectTextureRect->set_custom_minimum_size(Vector2(0,100));
    cvObjectTextureRect->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
    cvObjectTextureRect->set_v_size_flags(Control::SizeFlags::SIZE_EXPAND);

    parent->set_name("DepthAI_GUI");
    ScrollContainer* sc = (ScrollContainer*)get_node("ScrollContainer/depthAIPanel");
    Node* panel = get_node("ScrollContainer/depthAIPanel");
    panel->add_child(parent);
    parent->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
    parent->set_v_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);

    if (depthAIConfig.hasRGB){
        Label* l = Label::_new();
        l->set_text("RGB STREAM");
        parent->add_child(l);
        parent->add_child(rgbCamContainer);
        rgbCamContainer->add_child(rgbTextureRect);
        rgbCamContainer->add_child(rgbControlContainer);
    }
    if (depthAIConfig.hasMono){
        Label* l = Label::_new();
        l->set_text("MONO STREAMS");
        parent->add_child(l);
        parent->add_child(monoCamContainer);
        VBoxContainer* monoPreviewContainer = VBoxContainer::_new();
        monoPreviewContainer->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
        monoCamContainer->add_child(monoPreviewContainer);
        monoCamContainer->add_child(monoControlContainer);
        monoPreviewContainer->add_child(lMonoTextureRect);
        monoPreviewContainer->add_child(rMonoTextureRect);
    }
    if (depthAIConfig.hasDepth){
        Label* l = Label::_new();
        l->set_text("DEPTH STREAM");
        parent->add_child(l);
        parent->add_child(depthContainer);
        depthContainer->add_child(depthTextureRect);
        depthContainer->add_child(depthControlContainer);

    }

    if (depthAIConfig.hasFeatureTracking){
        Label* l = Label::_new();
        l->set_text("FEATURE TRACKING");
        parent->add_child(l);
        parent->add_child(featureTrackingContainer);
        featureTrackingContainer->add_child(cvFeatureTextureRect);
        featureTrackingContainer->add_child(featureTrackingControlContainer);

    }

    if (depthAIConfig.hasObjectTracking){
        Label* l = Label::_new();
        l->set_text("OBJECT TRACKING");
        parent->add_child(l);
        parent->add_child(objectTrackingContainer);
        objectTrackingContainer->add_child(cvObjectTextureRect);
        objectTrackingContainer->add_child(objectTrackingControlContainer);

    }

    Label* l = Label::_new();
    l->set_text("CALIBRATOR");
    parent->add_child(l);
    parent->add_child(calibrationControlContainer);

    for (uint32_t i=0;i<depthAIConfig.cameraControls.size();i++){
        CameraControl* cc = &depthAIConfig.cameraControls.at(i);

        HBoxContainer* controlBox = HBoxContainer::_new();
        controlBox->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
        Label* controlLabel = Label::_new();
        controlLabel->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
        controlLabel->set_text(cc->controlLabel.c_str());
        HSlider* controlSlider = HSlider::_new();
        controlSlider->set_h_size_flags(Control::SizeFlags::SIZE_EXPAND_FILL);
        controlSlider->set_name(cc->controlName.c_str());
        controlSlider->set_custom_minimum_size(Vector2(100,10));
        controlSlider->set_min(cc->min);
        controlSlider->set_max(cc->max);
        controlSlider->set_value(cc->defaultValue);
        controlSlider->set_focus_mode(Control::FOCUS_NONE);
        controlBox->add_child(controlLabel);
        controlBox->add_child(controlSlider);

        switch(cc->type){
        case CONTROL_TYPE_RGB_CONTROL:
            rgbControlContainer->add_child(controlBox);
            break;
        case CONTROL_TYPE_MONO_CONTROL:
            monoControlContainer->add_child(controlBox);
            break;
        case CONTROL_TYPE_DEPTH_CONFIG:
            depthControlContainer->add_child(controlBox);
            break;
        case CONTROL_TYPE_POINTCLOUD:
            depthControlContainer->add_child(controlBox);
            break;
        case CONTROL_TYPE_FEATURE_TRACKING:
            featureTrackingControlContainer->add_child(controlBox);
            break;
        default:
            printf("Unknown control type!\n");
            //do nothing
        }

        Array args;
        args.append(i); // this is the index into our array of camera controls
        controlSlider->connect("value_changed", this, "setControlSliderValue",args,0);


    }
}

void GDDepthAI::setControlSliderValue(float f,int i) {
    CameraControl* cc = &depthAIConfig.cameraControls.at(i);
    cc->setValue(this,f);

}

Ref<ImageTexture> GDDepthAI::getDepthAITex(String s) {
    if (s.begins_with_char_array("RGB")) {
            return daiRGBTex;
    }

    if (s.begins_with_char_array("LMono")) {
            return daiLMonoTex;
    }

    if (s.begins_with_char_array("RMono")) {
            return daiRMonoTex;
    }
    if (s.begins_with_char_array("Depth")) {
            return daiDepthTex;
    }

    if (s.begins_with_char_array("CVFeature")) {
            return cvFeatureDebugTex;
    }
    if (s.begins_with_char_array("CVObject")) {
            return cvObjectDebugTex;
    }
    if (s.begins_with_char_array("NNInput")) {
            return nnTex;
    }
    printf("Requested nonexistent Texture!\n");
    return Ref<ImageTexture>();

}

bool GDDepthAI::reconfigureDepthAI(DepthAIConfig* dc) {
    //if (daiThread){
    //    pthread_join(daiThread,NULL);
    //}
    //if (daiDevice && (! daiDevice->isClosed())){
    //    daiDevice->close();
   // }

    daiRGBQueue = nullptr;
    daiMonoQueue[0] = nullptr;
    daiMonoQueue[1] = nullptr;
    daiDepthQueue = nullptr;


    try {
             daiDevice = new dai::Device();
     } catch (std::runtime_error& e) {
        printf("Could not init depthai - no device?\n");
        depthaiConfigurationFailed = true;
        return false;
    }
    try {
        int fps=30;

        auto camRgb = pipeline.create<dai::node::ColorCamera>();
        camRgb->setBoardSocket(dai::CameraBoardSocket::RGB);
        camRgb->setResolution(dai::ColorCameraProperties::SensorResolution::THE_1080_P);
        camRgb->setColorOrder(dai::ColorCameraProperties::ColorOrder::RGB);
        camRgb->setPreviewSize(dc->rgbW,dc->rgbH);
        camRgb->setFps(fps);
        //camRgb->setIspScale(1, 1);
        auto rgbControlIn = pipeline.create<dai::node::XLinkIn>();
        auto rgbConfigIn = pipeline.create<dai::node::XLinkIn>();
        rgbControlIn->setStreamName("rgb_control");
        rgbConfigIn->setStreamName("rgb_config");
        rgbControlIn->out.link(camRgb->inputControl);
        rgbConfigIn->out.link(camRgb->inputConfig);

        auto camMono_0 = pipeline.create<dai::node::MonoCamera>();
        auto camMono_1 = pipeline.create<dai::node::MonoCamera>();
        camMono_0->setResolution(dai::MonoCameraProperties::SensorResolution::THE_400_P);
        camMono_0->setBoardSocket(dai::CameraBoardSocket::LEFT);
        camMono_0->setFps(fps);
        camMono_1->setResolution(dai::MonoCameraProperties::SensorResolution::THE_400_P);
        camMono_1->setBoardSocket(dai::CameraBoardSocket::RIGHT);
        camMono_1->setFps(fps);
        auto monoControlIn = pipeline.create<dai::node::XLinkIn>();
        monoControlIn->setStreamName("mono_control");
        monoControlIn->out.link(camMono_0->inputControl);
        monoControlIn->out.link(camMono_1->inputControl);

        dai::CalibrationHandler ch = daiDevice->readCalibration();
        daiMonoIntrinsics[0] = ch.getCameraIntrinsics(dai::CameraBoardSocket::LEFT,dc->monoW,dc->monoH);
        daiMonoIntrinsics[1] = ch.getCameraIntrinsics(dai::CameraBoardSocket::RIGHT,dc->monoW,dc->monoH);

        std::shared_ptr<dai::node::XLinkOut> rgbOut = pipeline.create<dai::node::XLinkOut>();
        rgbOut->setStreamName("rgb");
        if (dc->hasRGB) {
            camRgb->preview.link(rgbOut->input);
            daiRGBIntrinsics = ch.getCameraIntrinsics(dai::CameraBoardSocket::RGB,dc->rgbW,dc->rgbH);
            dc->rgbCameraParams.flat_intrinsics[0] = daiRGBIntrinsics[0][0];
            dc->rgbCameraParams.flat_intrinsics[1] = daiRGBIntrinsics[1][1];
            dc->rgbCameraParams.flat_intrinsics[2] = daiRGBIntrinsics[0][2];
            dc->rgbCameraParams.flat_intrinsics[3] = daiRGBIntrinsics[1][2];


        }
        auto monoOut_0 = pipeline.create<dai::node::XLinkOut>();
        auto monoOut_1 = pipeline.create<dai::node::XLinkOut>();
        monoOut_0->setStreamName("left");
        monoOut_1->setStreamName("right");

        if (dc->hasMono) {

            camMono_0->out.link(monoOut_0->input);
            camMono_1->out.link(monoOut_1->input);
            dc->lMonoCameraParams.flat_intrinsics[0] = daiMonoIntrinsics[0][0][0];
            dc->lMonoCameraParams.flat_intrinsics[1] = daiMonoIntrinsics[0][1][1];
            dc->lMonoCameraParams.flat_intrinsics[2] = daiMonoIntrinsics[0][0][2];
            dc->lMonoCameraParams.flat_intrinsics[3] = daiMonoIntrinsics[0][1][2];
            dc->rMonoCameraParams.flat_intrinsics[0] = daiMonoIntrinsics[1][0][0];
            dc->rMonoCameraParams.flat_intrinsics[1] = daiMonoIntrinsics[1][1][1];
            dc->rMonoCameraParams.flat_intrinsics[2] = daiMonoIntrinsics[1][0][2];
            dc->rMonoCameraParams.flat_intrinsics[3] = daiMonoIntrinsics[1][1][2];

        }
        auto stereo = pipeline.create<dai::node::StereoDepth>();
        stereo->setRuntimeModeSwitch(true);
        auto depthOut = pipeline.create<dai::node::XLinkOut>();
        auto depthConfig = pipeline.create<dai::node::XLinkIn>();
        depthOut->setStreamName("depth");
        depthConfig->setStreamName("depth_control");

        if (dc->hasDepth) {
            camMono_0->out.link(stereo->left);
            camMono_1->out.link(stereo->right);
            stereo->depth.link(depthOut->input);
            depthConfig->out.link(stereo->inputConfig);
            stereo->setDepthAlign(dai::CameraBoardSocket::RGB);
        }


        // Linking

        if (dc->hasMono && dc->hasFeatureTracking) {

            auto lTracker = pipeline.create<dai::node::FeatureTracker>();
            auto rTracker = pipeline.create<dai::node::FeatureTracker>();
            auto lTrackedFeatures = pipeline.create<dai::node::XLinkOut>();
            auto rTrackedFeatures = pipeline.create<dai::node::XLinkOut>();
            auto trackerConfigIn = pipeline.create<dai::node::XLinkIn>();

            lTrackedFeatures->setStreamName("l_tracked_features");
            rTrackedFeatures->setStreamName("r_tracked_features");
            trackerConfigIn->setStreamName("tracker_config");

            camMono_0->out.link(lTracker->inputImage);
            camMono_1->out.link(rTracker->inputImage);
            lTracker->outputFeatures.link(lTrackedFeatures->input);
            rTracker->outputFeatures.link(rTrackedFeatures->input);

            trackerConfigIn->out.link(lTracker->inputConfig);
            trackerConfigIn->out.link(rTracker->inputConfig);

            // By default the least mount of resources are allocated
            // increasing it improves performance when optical flow is enabled
            auto numShaves = 2;
            auto numMemorySlices = 2;
            lTracker->setHardwareResources(numShaves, numMemorySlices);
            rTracker->setHardwareResources(numShaves, numMemorySlices);

            auto trackerConfig = rTracker->initialConfig.get();
            printf("CONFIGURED FEATURE TRACKER\n");
        }

        if (dc->hasRGB && dc->hasObjectTracking) {

            std::shared_ptr<dai::node::ImageManip> downScaler = pipeline.create<dai::node::ImageManip>();
            std::shared_ptr<dai::node::MobileNetDetectionNetwork> detectionNetwork = pipeline.create<dai::node::MobileNetDetectionNetwork>();
            std::shared_ptr<dai::node::XLinkOut> trackedObjects = pipeline.create<dai::node::XLinkOut>();

            trackedObjects->setStreamName("tracked_objects");
            camRgb->preview.link(downScaler->inputImage);

            downScaler->initialConfig.setFrameType(dai::ImgFrame::Type::BGR888p);
            downScaler->initialConfig.setKeepAspectRatio(true);
            downScaler->initialConfig.setResize(depthAIConfig.nnConfig.inputW, depthAIConfig.nnConfig.inputH);

            downScaler->out.link(detectionNetwork->input);

            detectionNetwork->setConfidenceThreshold(0.5);
            detectionNetwork->setBlobPath(depthAIConfig.nnConfig.modelPath);
            detectionNetwork->setNumInferenceThreads(2);
            detectionNetwork->input.setBlocking(false);

            detectionNetwork->out.link(trackedObjects->input);




            printf("CONFIGURED OBJECT TRACKER\n");
        }

        daiDevice->startPipeline(pipeline);

        if (dc->hasRGB) {
            daiRGBQueue = daiDevice->getOutputQueue("rgb",1,false).get();
            daiRGBConfigQueue = daiDevice->getInputQueue("rgb_config",1,false).get();
            daiRGBControlQueue = daiDevice->getInputQueue("rgb_control",1,false).get();

        }

        if (dc->hasMono) {
            daiMonoQueue[0] = daiDevice->getOutputQueue("left",1,false).get();
            daiMonoQueue[1] = daiDevice->getOutputQueue("right",1,false).get();
            daiMonoControlQueue = daiDevice->getInputQueue("mono_control",1,false).get();

        }

        if (dc->hasDepth) {
            daiDepthQueue = daiDevice->getOutputQueue("depth",1,false).get();
            daiDepthControlQueue = daiDevice->getInputQueue("depth_control",1,false).get();
        }

        if (dc->hasFeatureTracking) {
            daiLTrackerQueue = daiDevice->getOutputQueue("l_tracked_features",1,false).get();
            daiRTrackerQueue = daiDevice->getOutputQueue("r_tracked_features",1,false).get();
            daiTrackerConfigQueue = daiDevice->getInputQueue("tracker_config",1,false).get();
        }

        if (dc->hasObjectTracking) {
            daiObjectTrackerQueue = daiDevice->getOutputQueue("tracked_objects",1,false).get();
        }


        //set our defaults

        for (uint32_t i=0;i<depthAIConfig.cameraControls.size();i++){
            CameraControl* cc = &depthAIConfig.cameraControls.at(i);
            cc->setValue(this,cc->defaultValue);
        }

        printf( "DEPTHAI: done setup!\n" );

    } catch (std::runtime_error& e) {
        printf("Could not init depthai - no device? %s\n", e.what());
        depthaiConfigurationFailed =true;

        //we check the validity of queues later in the code
        daiDepthControlQueue = nullptr;
        daiMonoControlQueue = nullptr;
        daiRGBConfigQueue = nullptr;
        daiRGBControlQueue = nullptr;
        daiTrackerConfigQueue=nullptr;
        daiRGBQueue = nullptr;
        daiMonoQueue[0]=nullptr;
        daiMonoQueue[1]=nullptr;
        daiRGBQueue = nullptr;
        daiLTrackerQueue = nullptr;
        daiRTrackerQueue = nullptr;
        daiObjectTrackerQueue = nullptr;
        return false;
    }

    daiLMonoPBA.resize(dc->monoW*dc->monoH);
    daiRMonoPBA.resize(dc->monoW*dc->monoH);
    daiDepthPBA.resize(dc->rgbW*dc->rgbH*DEPTH_BPP);
    daiRGBPBA.resize(dc->rgbW*dc->rgbH*RGB_BPP);
    cvFeatureDebugPBA.resize(dc->monoW*dc->monoH*RGB_BPP);
    cvObjectDebugPBA.resize(dc->rgbW*dc->rgbH*RGB_BPP);
    nnPBA.resize(dc->nnConfig.inputW*dc->nnConfig.inputH*RGB_BPP);

    daiLMonoTex->create(dc->monoW,dc->monoH,Image::FORMAT_L8);
    daiRMonoTex->create(dc->monoW,dc->monoH,Image::FORMAT_L8);
    daiDepthTex->create(dc->rgbW,dc->rgbH,Image::FORMAT_RG8);
    daiRGBTex->create(dc->rgbW,dc->rgbH,Image::FORMAT_RGB8);
    cvFeatureDebugTex->create(dc->monoW,dc->monoH,Image::FORMAT_RGB8);
    cvObjectDebugTex->create(dc->rgbW,dc->rgbH,Image::FORMAT_RGB8);
    nnTex->create(dc->nnConfig.inputW,dc->nnConfig.inputH,Image::FORMAT_RGB8);

    daiLMonoImg->create(dc->monoW,dc->monoH,false,Image::FORMAT_L8);
    daiRMonoImg->create(dc->monoW,dc->monoH,false,Image::FORMAT_L8);
    daiDepthImg->create(dc->rgbW,dc->rgbH,false,Image::FORMAT_RG8);
    daiRGBImg->create(dc->rgbW,dc->rgbH,false,Image::FORMAT_RGB8);
    cvFeatureDebugImg->create(dc->monoW,dc->monoH,false,Image::FORMAT_RGB8);
    cvObjectDebugImg->create(dc->rgbW,dc->rgbH,false,Image::FORMAT_RGB8);
    nnImg->create(dc->nnConfig.inputW,dc->nnConfig.inputH,false,Image::FORMAT_RGB8);

    depthaiConfigured = true;
    return true;
}

void GDDepthAI::drawStereoFeatureDebug(cv::Mat m,std::map<uint32_t,StereoFeature>* features) {
    std::map<uint32_t,StereoFeature>::iterator it;
    for (it = features->begin(); it != features->end();it++) {
        StereoFeature* f = &it->second;
        if (f->lostFrames == 0 && (f->history.size() > 10)) {
                cv::line(m,cv::Point2f(f->lPoint.x,f->lPoint.y),cv::Point2f(f->rPoint.x,f->rPoint.y),cv::Scalar(0,255,0),1);
                cv::circle(m,cv::Point2f(f->lPoint.x,f->lPoint.y),3,cv::Scalar(128,128,255),1);
                cv::circle(m,cv::Point2f(f->rPoint.x,f->rPoint.y),3,cv::Scalar(255,128,128),1);

                cv::putText(m,cv::format("%0.1f",f->disparity),cv::Point2f(f->rPoint.x,f->rPoint.y+10),cv::FONT_HERSHEY_SIMPLEX,0.4,cv::Scalar(64,255,64));

            }
    }
}

void GDDepthAI::positionMarkers(std::map<uint32_t,StereoFeature>* features) {
    for (uint32_t m=0;m<markers.size();m++) {
        markers[m]->set_visible(false);
    }
    std::map<uint32_t,StereoFeature>::iterator it;
    for (it = features->begin(); it != features->end();it++) {
        StereoFeature* f = &it->second;
        if (f->history.size() > 3){
            markers[f->markerIndex]->set_translation(f->cur.position);
            markers[f->markerIndex]->set_visible(true);
        }
    }
}


uint32_t GDDepthAI::calculateSAD(cv::Mat tmpl,cv::Mat match) {
  uint32_t sum=0;
  if (tmpl.cols ==0 || tmpl.rows == 0)
  {
      exit(0);
  }
  for (int i=0;i<tmpl.cols;i++) {
    for (int j=0;j<tmpl.rows;j++) {
      sum += abs((tmpl.at<uchar>(j,i) - match.at<uchar>(j,i)));
    }
  }
  return sum;
}


float GDDepthAI::subPixelRefine(cv::Mat tmpl,cv::Mat match,int dist){

    return dist;
}


void GDDepthAI::findCorrespondencesSAD(std::vector<dai::TrackedFeature>* rFeatures,std::map<uint32_t,StereoFeature>* out_features){
    //for each rFeature, we will search along the scanline for a matching feature in the lFrame
    //initially we will look at blocks every n pixels, and whichever one is closest we will do a
    //local area search to find the closest pixel match
    int32_t yRectificationShift = -4; //TODO - calculate on the fly
    uint32_t lrThresh = 5;
    uint32_t maxSearch=64;
    uint32_t patchSize=15;
    int32_t rectSearch=0; //we convert this to a negative offset during search so we use int32_t
    uint32_t halfPatch= floor(patchSize/2.0);

    cv::Mat cvLMat(depthAIConfig.monoH,depthAIConfig.monoW,CV_8UC1,(void*)daiLMonoPBA.read().ptr());
    cv::Mat cvRMat(depthAIConfig.monoH,depthAIConfig.monoW,CV_8UC1,(void*)daiRMonoPBA.read().ptr());

    std::map<uint32_t,StereoFeature>::iterator it;
    for (it = out_features->begin(); it != out_features->end(); it++) {
        StereoFeature* f = &it->second;
        f->updateHist = true;
        f->lostFrames++;
    }

    for (uint32_t i=0;i<rFeatures->size() ;i++) {
        dai::TrackedFeature rtf = rFeatures->at(i);
        //create our matching patch
        //ignore if we are too close to the edges
        if ((rtf.position.x > halfPatch) && (rtf.position.x < (depthAIConfig.monoW - maxSearch-halfPatch)) && (rtf.position.y > (halfPatch+rectSearch-yRectificationShift)) && (rtf.position.y < (depthAIConfig.monoH - halfPatch-rectSearch+yRectificationShift)))
        {
            uint32_t cSAD;
            uint32_t lowestSAD=UINT32_MAX;
            uint32_t lowestDist=UINT32_MAX;
            int32_t y_off;
            cv::Rect rRoi(rtf.position.x-halfPatch,rtf.position.y-halfPatch,patchSize,patchSize);
            cv::Rect subPixR(rtf.position.x-halfPatch,rtf.position.y,patchSize,1);
            uint32_t values[maxSearch];
            for (int32_t j=(-1 * rectSearch); j <= rectSearch;j++) {
                for (int32_t k=1; k<maxSearch; k++) {
                    cv::Rect lRoi(rtf.position.x-halfPatch + k,rtf.position.y-halfPatch + j + yRectificationShift,patchSize,patchSize);
                    cSAD = calculateSAD(cvRMat(rRoi),cvLMat(lRoi));

                    values[k] = cSAD;
                    if (cSAD < lowestSAD) {
                        lowestSAD = cSAD;
                        lowestDist = k;
                        y_off = j;
                    }
                }
            }

            if (lowestDist < UINT32_MAX) {
                //refine by finding the best matching patch in the reverse direction. if we don't
                //come up with close to the same number, reject this match.
                cv::Point2i lPos(rtf.position.x-halfPatch + lowestDist,rtf.position.y-halfPatch + yRectificationShift);
                cv::Rect lRoi(lPos.x,lPos.y,patchSize,patchSize);
                uint32_t rLowestSAD = UINT32_MAX;
                uint32_t rLowestDist = UINT32_MAX;
                uint32_t search = lowestDist+lrThresh;
                if (lPos.x < search){
                    search = lPos.x;
                }
                for (int32_t k=1; k<search; k++) {
                    cv::Rect rRoi(lPos.x -k ,lPos.y,patchSize,patchSize);
                    cSAD = calculateSAD(cvRMat(rRoi),cvLMat(lRoi));
                    if (cSAD < rLowestSAD) {
                        rLowestSAD = cSAD;
                        rLowestDist = k;
                    }
                }
                int diff = lowestDist-rLowestDist;
                if (abs(diff) > lrThresh) {
                    //printf("REJECTING! DIFF: %d\n",abs(diff));
                } else {
                    cv::Rect subPixL(rtf.position.x-halfPatch+lowestDist,rtf.position.y+y_off+yRectificationShift,patchSize,1);
                    float subPixelDist = subPixelRefine(cvRMat(subPixR),cvLMat(subPixL),lowestDist);
                    float depth = (depthAIConfig.rMonoCameraParams.flat_intrinsics[0] * depthAIConfig.monoBaseline) / subPixelDist;
                    float unitScale=16.;
                    Vector3 t_position = Vector3 (-unitScale*(rtf.position.x - depthAIConfig.rMonoCameraParams.flat_intrinsics[2]) * (depth / depthAIConfig.rMonoCameraParams.flat_intrinsics[0]),
                                              -unitScale*(rtf.position.y - depthAIConfig.rMonoCameraParams.flat_intrinsics[3]) * (depth / depthAIConfig.rMonoCameraParams.flat_intrinsics[1]),unitScale*depth);

                    it = out_features->find( rtf.id );
                    if (it != stereoFeatures.end()) {
                          StereoFeature* f= &it->second;
                          f->history.push_back(f->cur);
                          f->rId = rtf.id;
                          f->lPoint = Vector2(rtf.position.x+lowestDist,rtf.position.y);
                          f->rPoint = Vector2(rtf.position.x,rtf.position.y);
                          f->disparity = lowestDist;
                          f->depth = depth;
                          f->cur.position = t_position;
                          f->lostFrames=0;
                    } else {
                        //add a new entry to the map
                        StereoFeature sf;
                        sf.rId = rtf.id;
                        sf.lPoint = Vector2(rtf.position.x+lowestDist,rtf.position.y);
                        sf.rPoint = Vector2(rtf.position.x,rtf.position.y);
                        sf.depth = depth;
                        sf.disparity = lowestDist;
                        sf.cur.position = t_position;
                        sf.lostFrames = 0;

                        getFreeMarkerIndex(&sf.markerIndex);

                        out_features->emplace(std::pair<uint32_t,StereoFeature>(rtf.id,sf));
                    }
                }
            }
        }
    }
}


void GDDepthAI::_process(float delta) {

    if (! depthaiConfigured && ! depthaiConfigurationFailed){
        reconfigureDepthAI(&depthAIConfig);
        createPointcloudMesh(depthAIConfig.rgbW,depthAIConfig.rgbH);
        createTrackingMarkers(200);
        depthAIConfig.alignDepthToRGB = true;
    }

    if (depthAIConfig.hasRGB && daiRGBQueue) {
        std::vector<std::shared_ptr<dai::ImgFrame>> rgbFrames = daiRGBQueue->tryGetAll<dai::ImgFrame>();
        if (rgbFrames.size() > 0 ) {
            PoolByteArray::Write w = daiRGBPBA.write();
            memcpy(w.ptr(), rgbFrames.back()->getData().data(),depthAIConfig.rgbW*depthAIConfig.rgbH*RGB_BPP);
            daiRGBImg->create_from_data(depthAIConfig.rgbW,depthAIConfig.rgbH,false,Image::FORMAT_RGB8,daiRGBPBA);
            daiRGBTex->set_data(daiRGBImg);

        }
    }

    if (depthAIConfig.hasMono && daiMonoQueue[0] && daiMonoQueue[1]) {
        std::vector<std::shared_ptr<dai::ImgFrame>> lMonoFrames = daiMonoQueue[0]->tryGetAll<dai::ImgFrame>();
        if (lMonoFrames.size() > 0 ) {
            PoolByteArray::Write w = daiLMonoPBA.write();
            memcpy(w.ptr(), lMonoFrames.back()->getData().data(),depthAIConfig.monoW*depthAIConfig.monoH);
            daiLMonoImg->create_from_data(depthAIConfig.monoW,depthAIConfig.monoH,false,Image::FORMAT_L8,daiLMonoPBA);
            daiLMonoTex->set_data(daiLMonoImg);
        }
        std::vector<std::shared_ptr<dai::ImgFrame>> rMonoFrames = daiMonoQueue[1]->tryGetAll<dai::ImgFrame>();

        if (rMonoFrames.size() > 0 ) {
            PoolByteArray::Write w = daiRMonoPBA.write();
            memcpy(w.ptr(), rMonoFrames.back()->getData().data(),depthAIConfig.monoW*depthAIConfig.monoH);
            daiRMonoImg->create_from_data(depthAIConfig.monoW,depthAIConfig.monoH,false,Image::FORMAT_L8,daiRMonoPBA);
            daiRMonoTex->set_data(daiRMonoImg);

        }
    }

    if (depthAIConfig.hasFeatureTracking && daiLTrackerQueue && daiRTrackerQueue) {

        std::vector<dai::TrackedFeature> lTrackFeatures = daiLTrackerQueue->get<dai::TrackedFeatures>()->trackedFeatures;
        std::vector<dai::TrackedFeature> rTrackFeatures = daiRTrackerQueue->get<dai::TrackedFeatures>()->trackedFeatures;

        //copy our mono images into te debug cv::Mat for as our background
        const uint8_t* lMonoData = daiLMonoPBA.read().ptr();
        const uint8_t* rMonoData = daiRMonoPBA.read().ptr();
        uint8_t* data = cvFeatureDebugPBA.write().ptr();
        cvFeatureDebugMat = cv::Mat(depthAIConfig.monoH,depthAIConfig.monoW,CV_8UC3,(void*)data);
        cv::Mat cvLMonoMat(depthAIConfig.monoH,depthAIConfig.monoW,CV_8UC1,(void*)lMonoData);
        cv::Mat cvRMonoMat(depthAIConfig.monoH,depthAIConfig.monoW,CV_8UC1,(void*)rMonoData);
        std::vector<cv::Mat> images(3);
        images[0]=cvLMonoMat;
        images[1]=cvNullMonoMat;
        images[2]=cvRMonoMat;
        cv::merge(images,cvFeatureDebugMat);

        findCorrespondencesSAD(&rTrackFeatures,&stereoFeatures);

        //remove any expired features
        std::map<uint32_t,StereoFeature>::iterator it;
        for (it = stereoFeatures.begin(); it != stereoFeatures.end();) {
            StereoFeature* f = &it->second;
            if (f->lostFrames > 3) {
                it = stereoFeatures.erase(it);
                //printf("removing expired feature!\n");
            } else {
                it++;
            }
        }

        drawStereoFeatureDebug(cvFeatureDebugMat,&stereoFeatures);
        positionMarkers(&stereoFeatures);

        cvFeatureDebugImg->create_from_data(depthAIConfig.monoW,depthAIConfig.monoH,false,Image::FORMAT_RGB8,cvFeatureDebugPBA);
        cvFeatureDebugTex->set_data(cvFeatureDebugImg);

    }


    if (depthAIConfig.hasObjectTracking && daiObjectTrackerQueue) {


        //copy our rgb image into the debug cv::Mat for as our background
        uint8_t* data =  cvObjectDebugPBA.write().ptr();
        {
            const uint8_t* rgbData = daiRGBPBA.read().ptr();
            memcpy(data,rgbData,depthAIConfig.rgbW*depthAIConfig.rgbH*3);
        }
        cvObjectDebugMat = cv::Mat(depthAIConfig.rgbH,depthAIConfig.rgbW,CV_8UC3,(void*)data);


        std::vector<std::shared_ptr<dai::ImgDetections>> detectList = daiObjectTrackerQueue->tryGetAll<dai::ImgDetections>();
        if (detectList.size() > 0 ) {
            //printf("got %d object lists from nn\n",detectList.size());
            for (uint32_t i=0;i<detectList.size();i++){
                std::shared_ptr<dai::ImgDetections> detectObj = detectList.at(i);
                for (uint32_t j=0;j<detectObj->detections.size();j++) {
                    dai::ImgDetection& d = detectObj->detections.at(j);
                    printf("Found detection of label %s (%f) at roi %f %f %f %f\n",depthAIConfig.nnConfig.labels[d.label].c_str(),d.confidence,d.xmin,d.ymin,d.xmax,d.ymax);
                    cv::rectangle(cvObjectDebugMat,cv::Point2f(d.xmin * depthAIConfig.rgbW,d.ymin* depthAIConfig.rgbH),cv::Point2f(d.xmax* depthAIConfig.rgbW,d.ymax* depthAIConfig.rgbH),cv::Scalar(0,255,0),1);
                    cv::putText(cvObjectDebugMat,cv::format("%s",depthAIConfig.nnConfig.labels[d.label].c_str()),cv::Point2f(d.xmin * depthAIConfig.rgbW,d.ymin* depthAIConfig.rgbH),cv::FONT_HERSHEY_SIMPLEX,2.0,cv::Scalar(64,255,64));

                }
            }
        }

        cvObjectDebugImg->create_from_data(depthAIConfig.rgbW,depthAIConfig.rgbH,false,Image::FORMAT_RGB8,cvObjectDebugPBA);
        cvObjectDebugTex->set_data(cvObjectDebugImg);

    }

    if (depthAIConfig.hasDepth && daiDepthQueue) {
        std::vector<std::shared_ptr<dai::ImgFrame>> depthFrames = daiDepthQueue->tryGetAll<dai::ImgFrame>();
        if (depthFrames.size() > 0 ) {
            uint32_t dw=depthAIConfig.monoW;
            uint32_t dh=depthAIConfig.monoH;

            if (depthAIConfig.alignDepthToRGB){
                dw = depthAIConfig.rgbW;
                dh = depthAIConfig.rgbH;
            }

            PoolByteArray::Write w = daiDepthPBA.write();
            memcpy(w.ptr(), depthFrames.back()->getData().data(),dw*dh*DEPTH_BPP);
            daiDepthImg->create_from_data(dw,dh,false,Image::FORMAT_RG8,daiDepthPBA);
            daiDepthTex->set_data(daiDepthImg);
        }
    }
}

int rawToISO(float f) {
    switch(int(f)){
    case 0:
        return 100;
    case 1:
        return 200;
    case 2:
        return 400;
    case 3:
        return 800;
    default:
        return 1600;
    }
}

dai::StereoDepthConfig::MedianFilter rawToDepthMedianKernel(float f){
    switch(int(f)){
     case 1:
        return dai::StereoDepthConfig::MedianFilter::KERNEL_3x3;
    case 2:
        return dai::StereoDepthConfig::MedianFilter::KERNEL_5x5;
    case 3:
        return dai::StereoDepthConfig::MedianFilter::KERNEL_7x7;
    default:
        return dai::StereoDepthConfig::MedianFilter::MEDIAN_OFF;
    }
}

dai::RawStereoDepthConfig::PostProcessing::TemporalFilter::PersistencyMode rawToPersistencyMode(float f){
    switch(int(f)){
        case 1:
            return dai::RawStereoDepthConfig::PostProcessing::TemporalFilter::PersistencyMode::VALID_1_IN_LAST_2;
        case 2:
            return dai::RawStereoDepthConfig::PostProcessing::TemporalFilter::PersistencyMode::VALID_1_IN_LAST_5;
        case 3:
            return dai::RawStereoDepthConfig::PostProcessing::TemporalFilter::PersistencyMode::VALID_1_IN_LAST_8;
        case 4:
            return dai::RawStereoDepthConfig::PostProcessing::TemporalFilter::PersistencyMode::VALID_2_IN_LAST_3;
        case 5:
            return dai::RawStereoDepthConfig::PostProcessing::TemporalFilter::PersistencyMode::VALID_2_IN_LAST_4;
        case 6:
            return dai::RawStereoDepthConfig::PostProcessing::TemporalFilter::PersistencyMode::VALID_2_OUT_OF_8;
        case 7:
            return dai::RawStereoDepthConfig::PostProcessing::TemporalFilter::PersistencyMode::VALID_8_OUT_OF_8;
        case 8:
            return dai::RawStereoDepthConfig::PostProcessing::TemporalFilter::PersistencyMode::PERSISTENCY_INDEFINITELY;
    default:
        return dai::RawStereoDepthConfig::PostProcessing::TemporalFilter::PersistencyMode::PERSISTENCY_OFF;
    }
}

bool rawToBool(float f){
    switch (int(f)) {
            case 1:
                return true;
            default: return false;
    }
}

CameraControl::CameraControl(ControlType _t, ControlId _id,float _min, float _max,float _steps,float _default,std::string _controlName,std::string _controlLabel,std::string _valueLabel){
    type=_t;
    id = _id;
    value= _default;
    min=_min;
    max=_max;
    steps=_steps;
    defaultValue=_default;
    controlName=_controlName;
    controlLabel=_controlLabel;
}

float CameraControl::getValue() {
    return value;
}

void CameraControl::setValue(GDDepthAI* d,float f) {
    value=f;
    switch (type) {
        case CONTROL_TYPE_RGB_CONTROL: {
                dai::CameraControl cc;
                switch (id) {
                    case CONTROL_ID_EXP_MS:
                        d->depthAIConfig.rgbCameraParams.exposureMs = f;
                        cc.setManualExposure(d->depthAIConfig.rgbCameraParams.exposureMs,rawToISO(d->depthAIConfig.rgbCameraParams.exposureISO));
                        break;
                    case CONTROL_ID_EXP_ISO:
                        d->depthAIConfig.rgbCameraParams.exposureISO = f;
                        cc.setManualExposure(d->depthAIConfig.rgbCameraParams.exposureMs,rawToISO(d->depthAIConfig.rgbCameraParams.exposureISO));
                        break;
                default:
                    printf("Unknown Control ID!\n");
                }
                d->daiRGBControlQueue->send(cc);
            }
            break;
        case CONTROL_TYPE_MONO_CONTROL: {
                dai::CameraControl cc;
                switch (id) {
                    case CONTROL_ID_EXP_MS:
                        d->depthAIConfig.lMonoCameraParams.exposureMs = f;
                        d->depthAIConfig.rMonoCameraParams.exposureMs = f;
                        cc.setManualExposure(d->depthAIConfig.rMonoCameraParams.exposureMs,rawToISO(d->depthAIConfig.rMonoCameraParams.exposureISO));
                        break;
                    case CONTROL_ID_EXP_ISO:
                        d->depthAIConfig.lMonoCameraParams.exposureISO = f;
                        d->depthAIConfig.rMonoCameraParams.exposureISO = f;
                        cc.setManualExposure(d->depthAIConfig.rMonoCameraParams.exposureMs,rawToISO(d->depthAIConfig.rMonoCameraParams.exposureISO));
                        break;
                    default:
                        printf("Unknown Control ID!\n");
                }
                d->daiMonoControlQueue->send(cc);
            }
            break;
        case CONTROL_TYPE_DEPTH_CONFIG: {
                dai::StereoDepthConfig cc;
                switch (id) {
                    case CONTROL_ID_DEPTH_CONF:
                        d->depthAIConfig.depthCameraParams.confidence = f;
                        break;
                    case CONTROL_ID_DEPTH_MEDIAN:
                        d->depthAIConfig.depthCameraParams.median_filter = f;
                        break;
                    case CONTROL_ID_DEPTH_SUBPIXEL:
                        d->depthAIConfig.depthCameraParams.subpixel = f;
                        break;
                    case CONTROL_ID_DEPTH_CENSUS_MEAN_ENABLE:
                        d->depthAIConfig.depthCameraParams.census_mean = f;
                        break;
                    case CONTROL_ID_DEPTH_TEMPORAL_FILTER:
                        d->depthAIConfig.depthCameraParams.temporal_filter = f;
                        break;
                    case CONTROL_ID_DEPTH_THRESHOLD_MIN:
                        d->depthAIConfig.depthCameraParams.threshold_filter_min = f;
                        break;
                    case CONTROL_ID_DEPTH_THRESHOLD_MAX:
                        d->depthAIConfig.depthCameraParams.threshold_filter_max = f;
                        break;
                    case CONTROL_ID_DEPTH_LR_CHECK:
                        d->depthAIConfig.depthCameraParams.lrcheck = f;
                        break;
                    case CONTROL_ID_DEPTH_SPATIAL_RADIUS:
                        d->depthAIConfig.depthCameraParams.spatial_radius = f;
                        break;
                    case CONTROL_ID_DEPTH_SPATIAL_ITERATIONS:
                        d->depthAIConfig.depthCameraParams.spatial_iterations = f;
                        break;
                    case CONTROL_ID_DEPTH_SPECKLE:
                        d->depthAIConfig.depthCameraParams.speckle_range = f;
                        break;
                    case CONTROL_ID_DEPTH_DECIMATION:
                        d->depthAIConfig.depthCameraParams.decimation_factor = f;
                        break;
                    default:
                        printf("Unknown Control ID!\n");
                }
                //all config needs to be set in a single message
                //set raw-accessible fields first
                dai::RawStereoDepthConfig r;

                r.censusTransform.enableMeanMode = false;
                if (d->depthAIConfig.depthCameraParams.census_mean){
                    r.censusTransform.enableMeanMode = true;
                }

                r.postProcessing.temporalFilter.enable = false;
                if (d->depthAIConfig.depthCameraParams.temporal_filter > 0){
                    r.postProcessing.temporalFilter.enable = true;
                    r.postProcessing.temporalFilter.persistencyMode = rawToPersistencyMode(d->depthAIConfig.depthCameraParams.temporal_filter);
                }

                r.postProcessing.spatialFilter.enable = false;
                if (d->depthAIConfig.depthCameraParams.spatial_radius > 0){
                    r.postProcessing.spatialFilter.enable = true;
                    r.postProcessing.spatialFilter.holeFillingRadius = d->depthAIConfig.depthCameraParams.spatial_radius;
                    r.postProcessing.spatialFilter.numIterations = d->depthAIConfig.depthCameraParams.spatial_iterations;
                }
                r.postProcessing.speckleFilter.enable = false;
                if (d->depthAIConfig.depthCameraParams.speckle_range > 0){
                    r.postProcessing.speckleFilter.enable = true;
                    r.postProcessing.speckleFilter.speckleRange = d->depthAIConfig.depthCameraParams.speckle_range;
                }
                //cant reconfigure decimation when rgb-align is on
                //r.postProcessing.decimationFilter.decimationFactor = d->depthAIConfig.depthCameraParams.decimation_factor;
                r.postProcessing.thresholdFilter.minRange = d->depthAIConfig.depthCameraParams.threshold_filter_min;
                r.postProcessing.thresholdFilter.maxRange = d->depthAIConfig.depthCameraParams.threshold_filter_max;


                cc.set(r);
                //cant turn off lr check with rgb-align is on
                cc.setLeftRightCheck(true);
                cc.setLeftRightCheckThreshold(d->depthAIConfig.depthCameraParams.lrcheck);

                cc.setSubpixel(false);
                if (d->depthAIConfig.depthCameraParams.subpixel > 0){
                    cc.setSubpixel(true);
                }
                cc.setConfidenceThreshold(d->depthAIConfig.depthCameraParams.confidence);
                cc.setMedianFilter(rawToDepthMedianKernel(d->depthAIConfig.depthCameraParams.median_filter));

                d->daiDepthControlQueue->send(cc);
            }
            break;
        case CONTROL_TYPE_POINTCLOUD: {
            switch (id) {
                case CONTROL_ID_POINTCLOUD_POINTSIZE:
                    if (! d->pointcloudShaderMaterial.is_null()) {
                        d->pointcloudShaderMaterial->set_shader_param("point_scale",f);
                    }
                    break;
                default:
                    printf("Unknown Control ID!\n");
            }
        }
        break;
        case CONTROL_TYPE_FEATURE_TRACKING: {
            switch (id) {
                case CONTROL_ID_FEATURE_TRACKING_ALGORITHM:
                    printf("setting tracking algorithm: %f\n",f);
                    break;
                default:
                    printf("Unknown Control ID!\n");
            }
        }
        break;
        case CONTROL_TYPE_OBJECT_TRACKING: {
            switch (id) {
                case CONTROL_ID_OBJECT_TRACKING_ROI:
                    printf("setting object tracker roi: %f\n",f);
                    break;
                default:
                    printf("Unknown Control ID!\n");
            }
        }
        break;
        default:
            printf("Unknown Control Type!\n");
    }
}

bool GDDepthAI::getFreeMarkerIndex(uint32_t* out_i) {
    if (markers.size() ==0){
        *out_i=0;
        return false;
    }
    std::vector<uint32_t> freeList = freeMarkerList;
    std::map<uint32_t,StereoFeature>::iterator it;
    for (it = stereoFeatures.begin(); it != stereoFeatures.end(); it++){
        StereoFeature* sf = &it->second;
        if (sf->markerIndex < markers.size()){
            freeList[sf->markerIndex] = UINT32_MAX;
        }
    }
    for (uint32_t i=0;i<freeList.size();i++){
        if (freeList[i]< UINT32_MAX){
            *out_i=freeList[i];
            return true;
        }
    }
    *out_i=0;
    return false;
}

void GDDepthAI::createTrackingMarkers(uint32_t n) {
    Node* pointcloudRoot = get_node("depthAIPointcloud");
    Ref<CubeMesh> m = Ref<CubeMesh>(CubeMesh::_new());
    Ref<SpatialMaterial> trackingMarkerMaterial = Ref<SpatialMaterial>(SpatialMaterial::_new());
    trackingMarkerMaterial->set_albedo(Color(0.0,1.0,0.0,1.0));
    for (uint32_t i=0;i<n;i++){
        //create our mesh ad add it to our list
        MeshInstance* mi  = MeshInstance::_new();
        mi->set_mesh(m);
        mi->set_surface_material(0,trackingMarkerMaterial);
        mi->set_scale(Vector3(0.1,0.1,0.1));
        mi->set_translation(Vector3(0.0,0.0,0.0));
        mi->set_visible(false); //set invisible by default
        pointcloudRoot->add_child(mi);
        markers.push_back(mi);
        freeMarkerList.push_back(i);
    }

}

void GDDepthAI::createPointcloudMesh(uint32_t w, uint32_t h){
    PoolVector3Array vertices;
    vertices.resize(w*h*6);

    for (uint32_t i=0;i<w;i++)
    {
        for (uint32_t j=0;j<h;j++)
        {
            // quad vertex id - 0: -1,-1 1: -1,1 2: 1,1 3: 1,-1
            vertices.push_back(Vector3(i,j,0));
            vertices.push_back(Vector3(i,j,1));
            vertices.push_back(Vector3(i,j,2));

            vertices.push_back(Vector3(i,j,0));
            vertices.push_back(Vector3(i,j,2));
            vertices.push_back(Vector3(i,j,3));
        }
    }

    ArrayMesh* m = ArrayMesh::_new();
    Array arrays;
    arrays.resize(ArrayMesh::ARRAY_MAX);
    arrays[ArrayMesh::ARRAY_VERTEX] = vertices;
    m->add_surface_from_arrays(Mesh::PRIMITIVE_TRIANGLES, arrays);
    m->set_custom_aabb(AABB( Vector3(-32768.0,-32768.0,-32768.0),Vector3(65536.0,65536.0,65536.0)));
    pointcloudQuads = MeshInstance::_new();
    pointcloudQuads->set_mesh(m);
    pointcloudShaderMaterial = Ref<ShaderMaterial>(ShaderMaterial::_new());
    Ref<Shader> sh = Ref<Shader>(ResourceLoader::get_singleton()->load("depthai_pointcloud_shader.tres"));
    pointcloudShaderMaterial->set_shader(sh);
    pointcloudShaderMaterial->set_shader_param("texture_albedo",daiRGBTex);
    pointcloudShaderMaterial->set_shader_param("texture_depth",daiDepthTex);
    pointcloudShaderMaterial->set_shader_param("dim",Vector2(w,h));
    pointcloudShaderMaterial->set_shader_param("point_scale",3);
    float* intr = depthAIConfig.rgbCameraParams.flat_intrinsics;
    //godot has no Vector4, which seems.. criminally insane?
    pointcloudShaderMaterial->set_shader_param("intr",Plane(intr[0],intr[1],intr[2],intr[3]));
    pointcloudQuads->set_surface_material(0,pointcloudShaderMaterial);
    pointcloudQuads->_mesh_changed();

    if (! meshInTree) {
        Node* pointcloudRoot = get_node("depthAIPointcloud");
        if (pointcloudRoot) {
            pointcloudRoot->add_child(pointcloudQuads);
            meshInTree =true;
        }
    }

}


